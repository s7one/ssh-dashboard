# SSH-Dashboard with nginx reverse proxy

This folder contains a working example of a containerized ssh-dashboard, with [nginx](https://www.nginx.com/) put in front of it as a reverse proxy.

```
┌────────┐  ┌────────┐         ┌────────┐
│  Host  │  │  Host  │   ...   │  Host  │
└───┬────┘  └───┬────┘         └───┬────┘
    │SSH        │SSH               │SSH
    │           │                  │
    └───────┐   │       ┌──────────┘
            │   │       │
            │   │  ...  │
            ▼   ▼       ▼
    ┌─────────────────────────────┐
    │        ssh-dashboard        │
    └──────────────┬──────────────┘
                   │
             HTTP  │  Website /
                   │  REST-API
                   ▼
               ┌───────┐
               │ NGINX │
               └───┬───┘
                   │
      HTTPS and/or │
       Basic Auth  │
                   ▼

                Clients
```

## How to build

*Should* be easy as cake, provided you have docker and docker-compose ready:

```
docker-compose build
```

**Note** If you want to use https and/or http basic authentication, make sure to edit *nginx.conf* accordingly before building. Otherwise, you'll have to re-run the build because *nginx.conf* gets baked into the nginx image during the build process.

## How to run

Almost as easy:

```
docker-compose up
```

## Configuration

### HTTPS

* Uncomment the *HTTPS port and cert files* section in *nginx.conf*
* (Re-)build the nginx image
* Make sure the *domain.crt/.key* bind-mount in docker-compose.yml points to valid cert files

### HTTP Basic Authentication

HTTP basic auth is an easy way to protect access to the webserver (both websites and REST API) with a simple login.

#### How to enable

* Uncomment the *HTTP Basic Auth Settings* section in *nginx.conf*
* (Re-)build the nginx image
* Make sure the *htpasswd* bind-mount in docker-compose.yml points to a valid htpasswd file
* The username/password of the provided example file is "admin/admin"

#### How to define your own htpasswd file

To create a new file with a password for the user 'admin':

```
htpasswd -c htpasswd admin
<type in password>
```

*htpasswd* is part of the apache2 utils.

See: https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/
