# About SSH-Dashboard

SSH-Dashoard is an easy to set-up, basic Linux host monitoring solution which works without the need to install any additional software on the hosts. All host metrics are collected over a non-superuser ssh account.

Host status information are then available as

 * Websites and/or REST API from an integrated webserver
 * Prometheus target

 Additionally, SSH-Dashboard can actively

 * Publish to a MQTT broker
 * Write to an InfluxDb server

For more details, see the [documentation](https://s7one.gitlab.io/ssh-dashboard/)
