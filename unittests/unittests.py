#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import unittest
from unittest.mock import MagicMock
from unittest.mock import patch
#sys.path.append('../src/sshdashboard/')
sys.path.append('../src/')
from sshdashboard import mqttclient
from sshdashboard import systeminfoentry
from sshdashboard import metrics
from sshdashboard import appconfig
from sshdashboard import observer
from sshdashboard import observerbase
from sshdashboard import connector

class testinfoentry(unittest.TestCase):
    def setUp(self):
        self.entry = systeminfoentry.cEntry()

    def test_checklimits_sysload(self):
        self.entry.SetSysLoad(1.0)
        self.entry.SetCPU("","", 2)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_1min"], False)
        self.entry.SetSysLoad(2.0)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_1min"], False)
        self.entry.SetSysLoad(3.0)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_1min"], True)
        self.entry.SetSysLoad(3.0, 1.0, 1.0)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_1min"],  True)
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_5min"],  False)
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_15min"], False)
        self.entry.SetSysLoad(3.0, 2.0, 2.1)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_1min"],  True)
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_5min"],  False)
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_15min"], True)
        self.entry.SetSysLoad(1.0, 2.1, 2.0)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_1min"],  False)
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_5min"],  True)
        self.assertEqual(self.entry.ToDict()["SysLoadWarning_15min"], False)

    def test_checklimits_uptime(self):
        self.entry.SetUptime(0)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["HostDown"], True)
        self.entry.SetUptime(1)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["HostDown"], False)

    def test_checklimits_mem(self):
        self.entry.SetMem(90, 0, 10, 100, 10)
        self.entry.SetMemWarnPct(0.1, 0.1)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["MemLimitWarning"], False)
        self.assertEqual(self.entry.ToDict()["SwapLimitWarning"], False)
        self.entry.SetMem(90, 0, 8, 100, 9)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["MemLimitWarning"], True)
        self.assertEqual(self.entry.ToDict()["SwapLimitWarning"], True)

    def test_checklimits_fs(self):
        self.entry.SetFsInfo("/dev/test", 100, 10, 10)
        self.entry.SetFsWarnPct(0.1)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["RootFsLimitWarning"], False)
        self.entry.SetFsWarnPct(0.2)
        self.entry.CheckLimits()
        self.assertEqual(self.entry.ToDict()["RootFsLimitWarning"], True)

class testmqttclient(unittest.TestCase):
    class retcode:
        def __init__(self, ret = 0):
            self.rc = ret

    def setUp(self):
        self.client = mqttclient.cMqttClient(a_sBroker = 'mybroker',
                                             a_nPort = 1883,
                                             a_sUser = 'myuser',
                                             a_sPassword = 'mypassword',
                                             a_sCa_certs = "myca",
                                             a_sCertfile = "mycert",
                                             a_sKeyfile = "mykeyfile",
                                             a_sTopicPrefix = "myprefix")

    @patch('paho.mqtt.client.Client.tls_set')
    @patch('paho.mqtt.client.Client.connect')
    @patch('paho.mqtt.client.Client.loop_start')
    def test_start_with_tls(self, loop_start_mock, connect_mock, tls_set_mock):
        self.client.start()
        tls_set_mock.assert_called_with(ca_certs="myca", certfile="mycert", keyfile="mykeyfile")
        connect_mock.assert_called_with('mybroker', 1883)
        loop_start_mock.assert_called()

    @patch('paho.mqtt.client.Client.tls_set')
    @patch('paho.mqtt.client.Client.connect')
    @patch('paho.mqtt.client.Client.loop_start')
    def test_start_without_tls(self, loop_start_mock, connect_mock, tls_set_mock):
        self.client.__init__()
        self.client.start()
        tls_set_mock.assert_not_called()
        connect_mock.assert_not_called()
        loop_start_mock.assert_not_called()

    @patch('paho.mqtt.client.Client.disconnect')
    @patch('paho.mqtt.client.Client.loop_stop')
    def test_stop(self, loop_stop_mock, disconnect_mock):
        self.client.stop()
        loop_stop_mock.assert_called()
        disconnect_mock.assert_called()

    def test_publish_not_connected(self):
        self.assertEqual(self.client.publish('foo', 'bar'), False)

    @patch('paho.mqtt.client.Client.publish')
    def test_publish_connected_success(self, publish_mock):
        publish_mock.return_value = self.retcode(0)
        self.client.bConnected = True
        rc = self.client.publish('foo', 'bar')
        self.assertEqual(rc, True)
        publish_mock.assert_called_with('myprefix' + 'foo', 'bar', 0)

    @patch('paho.mqtt.client.Client.publish')
    def test_publish_connected_fail(self, publish_mock):
        publish_mock.return_value = self.retcode(1)
        self.client.bConnected = True
        rc = self.client.publish('foo', 'bar')
        self.assertEqual(rc, False)

    def test_on_connect(self):
        self.client._on_connect(None, None, None, 0)
        self.assertEqual(self.client.bConnected, True)
        self.client._on_connect(None, None, None, 1)
        self.assertEqual(self.client.bConnected, False)

    def test_on_disconnect(self):
        self.client._on_connect(None, None, None, 0)
        self.client._on_disconnect(None, None, 0)
        self.assertEqual(self.client.bConnected, False)

class testcollectorthread(unittest.TestCase):
        def setUp(self):
            hostinfo = {"sName": "myname",
                        "sHost": "myhost",
                        "nPort": 22,
                        "sUser": "myuser",
                        "sPassword" : "mypassword",
                        "sKeyFile": "mykeyfile",
                        "sCustomCmd": "nnn",
                        "sCustomCmdLabel": "",
                        "fMemWarnPct": 0.1,
                        "fSwapWarnPct": 0.1,
                        "fRootFsWarnPct": 0.1}
            self.collectorthread = metrics.collectorthread(hostinfo, connector.ssh(sName = hostinfo["sName"],
                                                                                           sHostname  = hostinfo["sHost"],
                                                                                           nPort      = hostinfo["nPort"],
                                                                                           sUser      = hostinfo["sUser"],
                                                                                           sPassword  = hostinfo["sPassword"],
                                                                                           sKeyfile   = hostinfo["sKeyFile"]))
            self.collectorthread.cmds =  { "df": "dummy", "lscpu": "dummy", "cat": "dummy"}

        @patch('paramiko.client.SSHClient.connect')
        @patch('os.access')
        def test_connect_password(self, access_mock, connect_mock):
            access_mock.return_value = True
            self.collectorthread.connectotarget()
            connect_mock.assert_called_with(hostname="myhost", port=22, username="myuser", key_filename="mykeyfile")

        @patch('paramiko.client.SSHClient.connect')
        @patch('os.access')
        def test_connect_keyfile(self, access_mock, connect_mock):
            access_mock.return_value = False
            self.collectorthread.connectotarget()
            connect_mock.assert_called_with(hostname="myhost", port=22, username="myuser", password="mypassword")

        @patch('paramiko.client.SSHClient.exec_command')
        def test_scrapeuptime(self, exec_mock):
            stdout = MagicMock()
            stdout.read().decode.return_value = "42.0 39758.04"
            exec_mock.return_value = (None, stdout, None)
            tinfo = systeminfoentry.cEntry()
            self.collectorthread.scrapeuptime(tinfo)
            self.assertEqual(tinfo.ToDict()["Uptime_s"], 42.0)

        @patch('paramiko.client.SSHClient.exec_command')
        def test_scrapeloadavg(self, exec_mock):
            stdout = MagicMock()
            stdout.read().decode.return_value = "0.64 0.74 0.5 1/906 7935"
            exec_mock.return_value = (None, stdout, None)
            tinfo = systeminfoentry.cEntry()
            self.collectorthread.scrapeloadavg(tinfo)
            self.assertEqual(tinfo.ToDict()["SysLoad_1min"], 0.64)

        @patch('paramiko.client.SSHClient.exec_command')
        def test_scrapememoryload(self, exec_mock):
            stdout = MagicMock()
            stdout.read().decode.return_value = "MemTotal:        3819348 kB\n\
                                                 MemFree:          376632 kB\n\
                                                 MemAvailable:    1317804 kB\n\
                                                 Buffers:          101560 kB\n\
                                                 Cached:          1267716 kB\n\
                                                 SwapCached:          916 kB\n\
                                                 Active:          1945152 kB\n\
                                                 Inactive:        1090148 kB\n\
                                                 SwapTotal:       2097148 kB\n\
                                                 SwapFree:        1993980 kB\n\
                                                 Dirty:                88 kB\n\
                                                 Writeback:             0 kB\n\
                                                 "
            exec_mock.return_value = (None, stdout, None)
            tinfo = systeminfoentry.cEntry()
            self.collectorthread.scrapememoryload(tinfo)
            tinfo.CheckLimits()
            d = tinfo.ToDict()
            self.assertEqual(d["MemTotal_kb"],      3819348)
            self.assertEqual(d["MemFree_kb"],       376632)
            self.assertEqual(d["MemAvailable_kb"],  1317804)
            self.assertEqual(d["MemLimitWarning"],  False)
            self.assertEqual(d["SwapTotal_kb"],     2097148)
            self.assertEqual(d["SwapFree_kb"],      1993980)
            self.assertEqual(d["SwapLimitWarning"], False)

        @patch('paramiko.client.SSHClient.exec_command')
        def test_scrapefsinfo(self, exec_mock):
            stdout = MagicMock()

            stdout.read().decode.return_value =  "Filesystem     1K-blocks     Used Available Use% Mounted on\n\
                                                  udev             3818100        0   3818100   0% /dev\n\
                                                  tmpfs             772652     2940    769712   1% /run\n\
                                                  /dev/sda2       75879228 12038644  59943036  17% /\n\
                                                  tmpfs            3863256        0   3863256   0% /dev/shm\n\
                                                  tmpfs               5120        0      5120   0% /run/lock\n\
                                                  tmpfs            3863256        0   3863256   0% /sys/fs/cgroup\n\
                                                  /dev/sda1         523248     5356    517892   2% /boot/efi\n\
                                                  "
            exec_mock.return_value = (None, stdout, None)
            tinfo = systeminfoentry.cEntry()
            self.collectorthread.scrapefsinfo(tinfo)
            d = tinfo.ToDict()
            self.assertEqual(d["RootFSPartition"],          "/dev/sda2")
            self.assertEqual(d["RootFSTotal_kb"],           75879228)
            self.assertEqual(d["RootFSAvailable_kb"],       59943036)
            self.assertEqual(d["RootFSAvailable_percent"],  83)

        @patch('paramiko.client.SSHClient.exec_command')
        def test_scrapecustomcmd(self, exec_mock):
            stdout = MagicMock()
            stdout.read().decode.return_value = "foobar"
            exec_mock.return_value = (None, stdout, None)
            tinfo = systeminfoentry.cEntry()
            self.collectorthread.scrapecustomcmd(tinfo)
            self.assertEqual(tinfo.ToDict()["CustomInfo"], "foobar")
            self.collectorthread.tHostInfo["sCustomCmd"] = ""
            self.collectorthread.scrapecustomcmd(tinfo)
            self.assertEqual(tinfo.ToDict()["CustomInfo"], "")

        @patch('paramiko.client.SSHClient.exec_command')
        def test_scrapecpuinfo(self, exec_mock):
            stdout = MagicMock()
            stdout.read().decode.return_value = "Address sizes:                   36 bits physical, 48 bits virtual\n\
                                                 CPU(s):                          4\n\
                                                 On-line CPU(s) list:             0-3\n\
                                                 Thread(s) per core:              2\n\
                                                 Core(s) per socket:              2\n\
                                                 Socket(s):                       1\n\
                                                 NUMA node(s):                    1\n\
                                                 Vendor ID:                       GenuineFoo\n\
                                                 CPU family:                      6\n\
                                                 Model:                           37\n\
                                                 Model name:                      my cpu\n\
                                                 Stepping:                        5\n\
                                                 "
            exec_mock.return_value = (None, stdout, None)
            tinfo = systeminfoentry.cEntry()
            self.collectorthread.scrapeonetimeinfo(tinfo)
            self.assertEqual(self.collectorthread.cpumodel, "my cpu")
            self.assertEqual(self.collectorthread.cpuvendor, "GenuineFoo")
            self.assertEqual(self.collectorthread.numcpus, 4)

        @patch('sshdashboard.observerbase.cSubjectBase.NotifyObservers')
        @patch('sshdashboard.metrics.collectorthread.scrapeuptime')
        @patch('sshdashboard.metrics.collectorthread.scrapeloadavg')
        @patch('sshdashboard.metrics.collectorthread.scrapememoryload')
        @patch('sshdashboard.metrics.collectorthread.scrapecustomcmd')
        @patch('sshdashboard.metrics.collectorthread.scrapeonetimeinfo')
        @patch('sshdashboard.metrics.collectorthread.scrapefsinfo')
        def test_scrapetarget(self, fsinfo_mock, onetime_mock, custom_mock, mem_mock, load_mock, uptime_mock, observer_mock):
            self.collectorthread.scrapetarget()
            onetime_mock.assert_called()
            custom_mock.assert_called()
            mem_mock.assert_called()
            load_mock.assert_called()
            uptime_mock.assert_called()
            observer_mock.assert_called()
            fsinfo_mock.assert_called()

class testappconfig(unittest.TestCase):
    def setUp(self):
        self.appconfig = appconfig.cConfig()

    def test_nofile(self):
        with self.assertRaises(FileNotFoundError):
            self.appconfig.ReadFromFile("doesnotexist")

    def test_dupidentities(self):
        with self.assertRaises(ValueError):
            self.appconfig.ReadFromFile("./config_dupidentities.json")

    def test_noidentities(self):
        with self.assertRaises(KeyError):
            self.appconfig.ReadFromFile("./config_noidentities.json")

    def test_unknownidentity(self):
        with self.assertRaises(ValueError):
            self.appconfig.ReadFromFile("./config_unknownidentity.json")

    def test_duphosts(self):
        with self.assertRaises(ValueError):
            self.appconfig.ReadFromFile("./config_duphosts.json")

    def test_nohosts(self):
        with self.assertRaises(KeyError):
            self.appconfig.ReadFromFile("./config_nohosts.json")

    def test_validfile(self):
        self.appconfig.ReadFromFile("./config_ok.json")
        self.assertEqual(len(self.appconfig.tHostList), 2)
        self.assertEqual(self.appconfig.tHostList[0]["sName"], "My Host 1")
        self.assertEqual(self.appconfig.tHostList[1]["sName"], "My Host 2")

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(testinfoentry))
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(testmqttclient))
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(testcollectorthread))
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(testappconfig))

    unittest.TextTestRunner(verbosity=2).run(suite)
    #unittest.main()
