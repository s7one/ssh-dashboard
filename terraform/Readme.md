# Terraform - Docker

If you do not want to use docker-compose or plain docker, you can also use terraform with the docker provider to build an start the containers. The subfolders contain examples for a plain single-container, and also for the nginx-reverse-proxy solution similar to the docker-compose examples. Probably won't see much use, but it was fun to do anyway.

## How to use

Pretty straightforward

```
terraform init
terraform apply
```
