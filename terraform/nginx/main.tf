terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.16.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "ssh-dashboard-image" {
  name         = "ssh-dashboard-image:latest"
  keep_locally = false
  build {
    path = "../.."
    label = {
      "org.label-schema.name" = "ssh-dashboard"
      "org.label-schema.version"= "1.3.0"
      "org.label-schema.schema-version" = "1.0"
    }
  }
}

resource "docker_image" "nginx-image" {
  name         = "nginx-image:latest"
  keep_locally = false
  build {
    path = "."
    dockerfile = "Dockerfile.nginx"
  }
}

resource "docker_container" "ssh-dashboard" {
  image = docker_image.ssh-dashboard-image.latest
  name  = "ssh-dashboard"
  restart = "always"
  networks_advanced {
    name = "sshdashboard"
  }
}

resource "docker_container" "nginx" {
  image = docker_image.nginx-image.latest
  name  = "nginx"
  restart = "always"
  networks_advanced {
    name = "sshdashboard"
  }
  ports {
    internal = 80
    external = 8000
  }
  depends_on = [docker_container.ssh-dashboard]
}

resource "docker_network" "sshdashboard" {
  name = "sshdashboard"
  driver = "bridge"
  ipam_config {
    subnet = "192.168.42.0/24"
  }
}
