terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.16.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "ssh-dashboard-image" {
  name         = "ssh-dashboard-image:latest"
  keep_locally = true
  build {
    path = "../.."
    label = {
      "org.label-schema.name" = "ssh-dashboard"
      "org.label-schema.version"= "1.3.0"
      "org.label-schema.schema-version" = "1.0"
    }
  }
}

resource "docker_container" "ssh-dashboard" {
  image = docker_image.ssh-dashboard-image.latest
  name  = "ssh-dashboard"
  restart = "always"
  networks_advanced {
    name = "sshdashboard"
  }
  ports {
    internal = 8080
    external = 8080
  }
  volumes {
    container_path = "/opt/ssh-dashboard/config.json"
    host_path = "${abspath(path.module)}/../../src/sshdashboard/config.json"
  }
}

resource "docker_network" "sshdashboard" {
  name = "sshdashboard"
  driver = "bridge"
  ipam_config {
    subnet = "192.168.42.0/24"
  }
}
