# Prometheus host metrics

SSH-Dashboard also works as a simple exporter for Prometheus metrics. It exposes most of the collected metrics from all hosts on the ```<server url>/prometheus``` endpoint. Using it this way, you could use a Prometheus/Grafana based monitoring setup without the need to install any exporters on the hosts.

## Sample metrics

Example metrics query for 2 hosts ```myhost1``` and ```myhost2```
```
HostDown{host="myhost1"} 0 1711704701000
Uptime_s{host="myhost1"} 4480195.63 1711704701000
SysLoad_1min{host="myhost1"} 0.05 1711704701000
SysLoad_5min{host="myhost1"} 0.04 1711704701000
SysLoad_15min{host="myhost1"} 0.01 1711704701000
MemTotal_kb{host="myhost1"} 7701816 1711704701000
MemFree_kb{host="myhost1"} 312600 1711704701000
MemAvailable_kb{host="myhost1"} 6880848 1711704701000
SwapTotal_kb{host="myhost1"} 4194300 1711704701000
SwapFree_kb{host="myhost1"} 4172284 1711704701000
RootFSTotal_kb{host="myhost1"} 75259748 1711704701000
RootFSAvailable_kb{host="myhost1"} 55226620 1711704701000
NumCPU{host="myhost1"} 4 1711704701000
HostDown{host="myhost2"} 0 1711704699000
Uptime_s{host="myhost2"} 423252.4 1711704699000
SysLoad_1min{host="myhost2"} 0.21 1711704699000
SysLoad_5min{host="myhost2"} 0.08 1711704699000
SysLoad_15min{host="myhost2"} 0.01 1711704699000
MemTotal_kb{host="myhost2"} 3882268 1711704699000
MemFree_kb{host="myhost2"} 1523060 1711704699000
MemAvailable_kb{host="myhost2"} 3362644 1711704699000
SwapTotal_kb{host="myhost2"} 0 1711704699000
SwapFree_kb{host="myhost2"} 0 1711704699000
RootFSTotal_kb{host="myhost2"} 61051912 1711704699000
RootFSAvailable_kb{host="myhost2"} 48498948 1711704699000
NumCPU{host="myhost2"} 4 1711704699000
```

## Prometheus configuration

Example Prometheus scrape configuration with SSH-Dashboard running on 192.168.1.42:8080
```
scrape_configs:

  - job_name: 'ssh-dashboard'
    scrape_interval: 5s
    scheme: http
    metrics_path: '/prometheus'
    static_configs:
        - targets: ['192.168.1.42:8080']
```
