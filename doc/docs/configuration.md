# Configuration

The default configuration file ```config.json``` bundles all information which the program needs to run.

## Sample configuration file

This is a rather minimal sample configuration, with only the integrated webserver active and two sample host entries. There is a more complete sample configuration provided in the repository.
```
{
  "webserver":
  {
    "localaddress": "0.0.0.0",
    "localport": 8080,
    "website_active": true,
    "restapi_active": true,
    "log_http_requests": false,
    "eventlog":
    {
      "logsize": 20,
      "keys"   :  [ "SysLoadWarning", "MemLimitWarning" ]
    }
  },

 "identities":
  [
    {
      "name": "my standard credentials",
      "ssh_user": "myusername",
      "ssh_privatekeyfile": "~/.ssh/mykey"
    }
  ],

  "hosts":
  [
    {
      "name": "My Host 1",
      "host": "myhost2",
      "identity": "my standard credentials",
      "scrape_interval_s": 4
    },
    {
      "name": "My Host 2",
      "host": "myhost2",
      "identity": "my standard credentials",
      "scrape_interval_s": 4
    }
  ]
}
```

## Configuration file reference


### ```webserver```

Holds the configuration for the integrated Flask webserver.

*```localaddress```*

(default: 0.0.0.0)<br>
The local ip address which the webserver binds to.

*```localport```*

(default: 8080)<br>
The local port where the integrated will listen for incoming connections.

*```website_active```*

(default: true)<br>
Controls whether the webserver should provide the scraped host information through a website.

*```restapi_active```*

(default: false)<br>
Controls whether the webserver should provide the scraped host information through a simple REST API. See Section "REST API".

**note**: The integrated webserver will only be started if at least one of the two options ```website_active``` and ```restapi_active``` is set to true.

*```log_http_requests```*

(default: true)<br>
Controls whether Http requests should be logged to the console. If the websites are polled regularly, turning this logs off will reduce the amount of produced log messages considerably. Error logging is unaffected.

---
### ```webserver/html_templates```

Holds the paths of the html templates. Normally, there is no need to change anything here.

---
### ```webserver/eventlog```

Configures the event log which can be viewed from the details website of a host.

*```logsize```*

(default: 30)<br>
The buffer size of the event log for each host. Older log entries are discarded for newer ones.

*```keys```*

(default: empty)<br>
Metrics which should be included in the log. This can be a list of keys from the complete host metrics json object (see REST API). Each change of a metric will generate a log entry, so a metric which changes often (such as ```SysLoad_1min```) will fill up the log buffer quickly. Warnings, e.g. ```SysLoadWarning``` are recommended candidates for logging. The ```HostDown``` key is always included in logging.

---
### ```mqttclient```

Holds the configuration for the integrated Mqtt client.

*```active```*

(default: false)<br>
Controls whether the Mqtt client is active.


*```broker```*

(default: "localhost")<br>
Broker hostname to connect to.

*```port```*

(default: 1883)<br>
Broker port to connect to.

*```user```*

(default: "")<br>
Username to use if the broker requires password based authentication.

*```password```*

(default: "")<br>
Password to use if the broker requires password based authentication.

*```ca_certs```*

(default: "")<br>
Path to trusted certificate authorities.

*```certfile```*

(default: "")<br>
Path to certificate to use for authentication.

*```keyfile```*

(default: "")<br>
Path to the keyfile.

*```topicprefix```*

(default: "/ssh-dashboard/hosts/")<br>
Prefix for the topics to which the host metrics will get pushed.

*```qos```*

(default: 0)<br>
Quality of service when publishing new data to the broker. Must be (0 | 1 | 2).

---
### ```influxdbclient```

Holds the configuration for the integrated InfluxDb client.

*```active```*

(default: false)<br>
Controls whether the InfluxDb client is active.

*```url```*

(default: "")<br>
The URL of the InfluxDb host to connect to.

*```organization```*

(default: "")<br>
Organization to use when writing to InfluxDb.

*```token```*

(default: "")<br>
Authentication token to use when writing to InfluxDb.

*```bucket```*

(default: "")<br>
InfluxDb bucket to write to.

*```timeout_ms```*

(default: 10000)<br>
Write timeout.

*```measurement_name```*

(default: "ssh-dashboard-metrics")<br>
Measurement name to use when writing to InfluxDb.


---
### ```identities```

Holds the array of identity entries. An identity describes user credentials for host authentication. If you use the same credentials for several hosts, you define it here once as an identity and re-use it when defining the hosts.



*```ssh_user```*

(mandatory)<br>
Username to use for the ssh connection.

*```ssh_password```*

(optional, default: ```""```)<br>
Password to use for password based authentication.

*```ssh_read_password_from_env```*

(optional, default: ```""```)<br>
If non-empty, then the password (the value of ```ssh_password```) will be read from the environment variable named like this string. The value read will overwrite a manually defined password.

*```ssh_read_password_from_tty```*

(optional, default: ```false```)<br>
If true, then the user will be prompted to input the password (the value of ```ssh_password```) on program startup . The value read will overwrite a manually defined password. Using this requires a tty attached to STDIN.

*```ssh_privatekeyfile```*

(optional, default:```""```)<br>
Path to the private key to use for public/private key based authentication. If both this and a password are specified, then the private key file will be used.

__Notes__:

* If both ```ssh_password``` and ```ssh_privatekeyfile``` are non-empty, then the private key file will be preferred.
* ```ssh_read_password_from_env``` and ```ssh_read_password_from_tty``` are mutually exclusive. Trying to use both at the same time for an identity will yield an error on startup.
* Storing clear-type passwords is considered **bad practice** (for good reasons) and discouraged. While it is possible to use clear-type passwords, it is highly recommended to use public/private key authentication instead. If you absolutely need to use password based authentication, make sure that the configuration file is only readable by its owner (e.g. chmod 600 on Linux) or pass the passwords using environment variables.


---
### ```hosts```

 Holds the array of host entries. Every entry represents one host. When defining a new host, some of the key/value pairs are mandatory, others are optional. A minimal entry looks like this:

 ```
{
  "name": "my host name",
  "host": "my host address",
  "identity": "my identity"
}
```

---
### Host entry key/value pairs overview

*```active```*

(optional, default: ```True```)<br>
If set to false, then this entry will completely ignored (as if it would be non-existing). Useful to temporarily exclude a host from being monitored, without having to delete the entry.

*```name```*

(mandatory)<br>
The name which will be shown to identify the host. Must be unique in the host entry array.

*```host```*

(mandatory)<br>
Hostname of ip address of the host.

*```identity```*

(mandatory)<br>
Identity to use for authentication with the host.

*```ssh_port```*

(optional, default: ```22```)<br>
Port where the remote ssh connection will be made to.

*```scrape_interval_s```*

(optional, default: ```5```)<br>
Interval of scraping the host, in seconds. A smaller value means more fine grained monitoring, but also leads to more traffic and cpu time used on the host.

*```memory_warning_pct```*

(optional, default: ```0.1```)<br>
If the available main memory drops below this percentage of the total memory, then the memory value will be shown in red to signal a warning.

*```swap_warning_pct```*

(optional, default: ```0.1```)<br>
If the available swap drops below this percentage of the total swap memory, then the swap value will be shown in red to signal a warning.

*```rootfs_warning_pct```*

(optional, default: ```0.1```)<br>
If the available free space on the root partition drops below this percentage of the total space of the root partition, then the rootfs value will be shown in red to signal a warning.


*```customcmd```*

(optional, default: ```""```)<br>
if non-empty, then this command will be executed directly on the remote host every it is scraped. The output of this command will be showed as-is on the respective hosts details page.
__Note__: Do not run very expensive commands, or programs that need interactive ttys here. Recommended are simple commands, which immediately return some formatted output. Example: ```customcmd: "df -h /"```

*```customcmdlabel```*

(optional, default: ```""```)<br>
Caption for the custom command, to be shown on the webpage.
