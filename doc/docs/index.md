# About SSH-Dashboard

SSH-Dashoard is a (deliberate) simple Linux host monitoring solution. It connects to each chosen host via ssh, and regularly collects basic informations (e.g.: uptime, system load and memory consumption) through the /proc filesystem. These metrics can be viewed through an integrated webserver, queried through a simple REST API or pushed to a MQTT broker or InfluxDb server for further processing.

SSH-Dashboard intends to be easy to use and requires little effort to setup, so it aims to be a KISS solution to show the current system health for limited number of hosts (although there is no hard limit on the number of hosts to be monitored). All you need is an active ssh account for each host to be monitored, and one single configuration file.

## How it works

In a nutshell:

![ssh-dashboard architecture](ssh-dashboard.svg)


 * Rather low dependencies (Python 3 and a bunch of libraries).
 * Setup without much hassle
 * Agentless - No need to install anything on the remote hosts besides a running SSH daemon.
 * Little requirements on the remote hosts. Most information are obtained from /proc, so chances are very high that it works just out-of-the box on every common linux distribution and architectures.


## Screenshots from the integrated websites

I am ***really not*** a front end developer, so this is as nice-looking as it gets for now :)

### Hosts overview
![ssh-dashboard website](ssh-dashboard-overview.png)

----

### Host details
![ssh-dashboard website](ssh-dashboard-details.png)

----

