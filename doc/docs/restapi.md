# REST API

If activated (see configuation file section), then the webserver provides host information through a minimalistic REST API. All responses are in JSON format.

## Host Ids

A list of all monitored hosts and their respective IDs can be obtained from  ```<server url>/api/hostids```. The IDs are needed to query host metrics.

### Example
```
{
  "Nuc 7": "4e75632037",
  "My RasPi4": "4d7920526173506934"
}
```

----
## Complete metrics for one host

The latest scraped host information can be acquired from ```<server url>/api/hosts/<id>```.

### Example

Querying ```<server url>/api/hosts/4e75632037```

May return something like this
```
{
  "Name": "Nuc 7",
  "HostDown": false,
  "Uptime_s": 824.92,
  "Timestamp": 1644266277,
  "LastContact": "07.02.2022 - 20:37:57 UTC",
  "SysLoad_1min": 0.12,
  "SysLoad_5min": 0.14,
  "SysLoad_15min": 0.09,
  "SysLoadWarning": false,
  "SysLoadWarning_1min": false,
  "SysLoadWarning_5min": false,
  "SysLoadWarning_15min": false,
  "MemTotal_kb": 7722040,
  "MemFree_kb": 6259840,
  "MemAvailable_kb": 6763128,
  "MemAvailable_percent": 87,
  "MemLimitWarning": false,
  "SwapTotal_kb": 4194300,
  "SwapFree_kb": 4194300,
  "SwapFree_percent": 100,
  "SwapLimitWarning": false,
  "RootFSPartition": "/dev/sda2",
  "RootFSTotal_kb": 75879228,
  "RootFSAvailable_kb": 57923752,
  "RootFSAvailable_percent": 80,
  "RootFsLimitWarning": false,
  "NumCPU": 4,
  "CPUVendor": "GenuineIntel",
  "CPUModel": "Intel(R) Pentium(R) Silver J5005 CPU @ 1.50GHz",
  "OSType": "Linux\n(\"Ubuntu 20.04.3 LTS\")",
  "OSVersion": "5.4.0-96-generic\n",
  "CustomInfo": "",
  "CustomLabel": ""
}
```

### notes

 * ```HostDown``` indicates whether ssh-dashboard does have a working ssh connection to this host. When there is no ssh connection (or when it gets interrupted), this host is considered to be down.
 * ```LastContact``` and ```Timestamp``` refer to the same metric: The last timepoint when metrics where successfully collected from this host. If a host is down (but was up and connected before), this shows the point in time when ssh-dashboard was able to collect metrics the last time before it went down.
 * ```SysLoadWarning_1min```, ```SysLoadWarning_5min``` and ```SysLoadWarning_15min``` gets set to true if the matching ```SysLoad_*``` value exceeds ```NumCPU```. There is no one single truth to tell when system load is too high, but relating sysload to the number of cpu threads is a quite common practice.
 * ```SysLoadWarning``` is **deprecated** and only included for backwards compatibility. It holds the same value as ```SysLoadWarning_1min```.
 * ```MemLimitWarning``` get set to true if the amount of available RAM (```MemAvailable_kb```) gets lower than the configured percentage of ```MemTotal_kb```. Defaults to 0.1 -> 10%.
 * ```SwapLimitWarning``` get set to true if the amount of available Swap (```SwapFree_kb```) gets lower than the configured percentage of ```SwapTotal_kb```. Defaults to 0.1 -> 10%.

----
## Single host metric

The latest scraped host information can be acquired from ```<server url>/api/hosts/<id>/<key>```. Key can be any key from the complete host metrics json object.

### Example

Querying ```<server url>/api/hosts/4e75632037/Uptime_s```

May return something like this
```
{"Uptime_s": 891.47}
```

----
## Hosts summary and IDs

A summary of some metrics for all hosts plus a list of all hosts and their respective IDs can be obtained from ```<server url>/api/summary```

### Example
```
{
  "hosts": {
    "Nuc 7": {
      "Name": "Nuc 7",
      "HostDown": false,
      "Uptime_s": 974.6,
      "Timestamp": 1644266277,
      "SysLoad_1min": 0.09,
      "SysLoad_5min": 0.13,
      "SysLoad_15min": 0.09,
      "SysLoadWarning_1min": false,
      "MemAvailable_percent": 87,
      "MemLimitWarning": false
    },
    "My RasPi4": {
      "Name": "My RasPi4",
      "HostDown": false,
      "Uptime_s": 1179.15,
      "Timestamp": 1644266277,
      "SysLoad_1min": 0.08,
      "SysLoad_5min": 0.07,
      "SysLoad_15min": 0.09,
      "SysLoadWarning_1min": false,
      "MemAvailable_percent": 90,
      "MemLimitWarning": false
    }
  },
  "hostids": {
    "Nuc 7": "4e75632037",
    "My RasPi4": "4d7920526173506934"
  }
}
```

----
## Event log

The event log for a single host can be acquired from ```<server url>/api/logs/hosts/<id>```. The response contains an ordered list of events.

### Example
```
[
  {
    "Name": "Nuc 7",
    "Key": "MemLimitWarning",
    "Value": true,
    "Timestamp": 1644266277
  },
  {
    "Name": "Nuc 7",
    "Key": "SysLoadWarning_5min",
    "Value": false,
    "Timestamp": 1644266277
  },
  {
    "Name": "Nuc 7",
    "Key": "HostDown",
    "Value": false,
    "Timestamp": 1644266277
  }
]
```
### notes

 * only the keys which are listed in the configuration file under *webserver/eventlog/keys* will be watched for changes.
