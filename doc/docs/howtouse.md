# How to use

There are several way how to setup a working ssh-dashboard installation:

## 1. Run SSH-Dashboard natively

1. Install via pip: ```pip install ssh-dashboard```
    * Note: You can also download the package from [GitLab](https://gitlab.com/s7one/ssh-dashboard/-/packages)
2. Get a sample configuration file with ```ssh-dashboard -f```
3. Edit ```config.json``` to suit your needs.
4. Run ```ssh-dashboard -c ./config.json``` to startup the local webserver (if configured) and also start scraping the hosts.

Note: ssh-dashboard has some dependencies, so it might be worth to install and run it in a virtual environment, like this:

```
python -m venv ssh-dashboard-venv
cd ssh-dasboard-venv
source ./bin/activate
python -m pip install ssh-dashboard
```


## 2. Build and run through Docker

You can build your own docker images from the repository. This way, there is no need to have Python, SSH or any other dependencies installed locally. You only need Docker (and optionally docker-compose).

*note: When building your own docker image, the configuration file src/config.json gets baked into the image with the contents it had when the image was built. The following steps show how to bind-mount your local config.json into the container.*

1. Clone/download this repo (obviously).
2. Edit ```src/ssh-dashboard/config.json``` to suit your needs.
3. From the repos root folder, run ```docker image build -t ssh-dashboard .```
4. From the repos root folder, run ```docker container run --rm -d -p 8080:8080 -v $PWD/src/ssh-dashboard/config.json:/opt/ssh-dashboard/config.json ssh-dashboard```

## 3. Use Docker with docker-compose

1. Clone/download this repo (obviously).
2. From the ```docker-compose/plain/``` subfolder:
2. Run ```docker-compose build```
    * This will bake the current ```src/ssh-dashboard/config.json``` into the image. However, this will not have much effect since docker-compose bind-mounts the current version of the file into the container everytime you run it.
3. Edit ```src/ssh-dashboard/config.json``` to suit your needs.
4. Run ```docker-compose up```

You can also find a working example of how to use nginx as a reverse proxy put in front of ssh-dashboards integrated webserver under ```docker-compose/nginx/```.

This is certainly closer to a 'production' ready solution, as it relies on nginx for delivering websites to the actual clients. HTTPS and/or protecting the websites and REST API with HTTP basic authentication can also easily be integrated this way. See the separate README in this folder for details.

