# InfluxDb client

If activated (see configuation file section), then SSH-Dashboard will connect to a InfluxDb server and write host metrics everytime it gets new data from a host.

## Topic

Host metrics will be published to the configured organization/bucket/measurement. Metrics are written in one measurement for each host everytime new data is acquired from that host. Each measurement gets tagged with a "Location"="Name" tag.


## Payload

Host metrics fields use the same keys/values as the REST API JSON payload.

## Example query

Using Flux query language:

```
from(bucket:"my-bucket") |> range(start: -15m) |> filter(fn:(r) => r._measurement == "ssh-dashboard-metrics") |> filter(fn: (r) => r.Location == "myhost1") |> filter(fn:(r) => r._field == "MemAvailable_kb")
```
