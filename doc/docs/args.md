## Arguments

### -v, --version
Show version

### -h, --help
Show help

### -c, --config

Path to the configuration file to use

### -f, --get_default_config

Place a sample configuration file (```config.json```) in the current working directory
