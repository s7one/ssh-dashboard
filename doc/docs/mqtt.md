# MQTT client

If activated (see configuation file section), then SSH-Dashboard will connect to a MQTT broker and publish new host metrics everytime it gets new data from this host.

## Topic

Host metrics will be published to ```<topicprefix>/<hostname>```, where ```<topicprefix>``` can be configured via the configuration file, and ```<hostname>``` is the content of the name-field from the respective hosts entry.

Example:
```
/ssh-dashboard/hosts/myhost1
```

## Payload

Host metrics are formatted in JSON and have the same content as the the "complete host metrics" payload from the REST API.
