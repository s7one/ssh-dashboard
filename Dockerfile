# Build image to get all the needed pip dependencies, no need to have pip and all the dev stuff in the final runtime image
FROM debian:bookworm as build

# enable non-interactive installation
ARG DEBIAN_FRONTEND=noninteractive

# install packages
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    python3-venv \
    build-essential \
    libffi-dev \
    libssl-dev \
 && rm -rf /var/lib/apt/lists/*

# install python dependencies
#RUN pip3 install --user --no-warn-script-location -r /opt/ssh-dashboard/requirements.txt
RUN pip3 install --break-system-packages setuptools build wheel

# copy python source code
COPY . /build/
WORKDIR /build

# build package
RUN ./deploy.sh

# Runtime image
FROM debian:bookworm as runtime

# enable non-interactive installation
ARG DEBIAN_FRONTEND=noninteractive

# install packages
RUN apt-get update && apt-get install -y \
    python3 python3-pip \
 && rm -rf /var/lib/apt/lists/*

# copy python package from build container
COPY --from=build /build/dist/ssh_dashboard-1.6.0-py3-none-any.whl /
RUN mkdir -p /opt/ssh-dashboard
COPY --from=build /build/src/sshdashboard/config.json /opt/ssh-dashboard/config.json

# install package
WORKDIR /
RUN pip3 install --break-system-packages ssh_dashboard-1.6.0-py3-none-any.whl

# set entrypoint
ENTRYPOINT ["ssh-dashboard", "-c", "/opt/ssh-dashboard/config.json"]
