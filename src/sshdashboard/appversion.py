#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def GetAppVersion() -> str:
    return "1.6.0"
