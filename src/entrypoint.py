#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sshdashboard.main

if __name__ == '__main__':
    exit(sshdashboard.main.run_program())
